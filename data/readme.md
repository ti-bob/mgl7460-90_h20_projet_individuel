# Méthodologie
Faire un clone du dépôt [Mockito](https://github.com/mockito/mockito)
```
$ git clone https://github.com/mockito/mockito.git
```
L'outil [GitStats](https://github.com/tomgi/git_stats) a été utilisé pour générer les données d'analyse.

Se positionner dans le répertoire du clone de Mockito puis exécuter GitStats
```
$ git_stats generate
```
un répertoire git_stats sera créer. Dans ce répertoire il y a le fichier index.html qui contient l'analyse.

 ## Installation

### Existing ruby/gem environment

```
$ gem install git_stats
```

### debian stretch (9.*)

```
# apt-get install ruby ruby-nokogiri ruby-nokogiri-diff ruby-nokogumbo
# gem install git_stats
```

### Ubuntu

```
$ sudo apt-get install ruby ruby-dev gcc zlib1g-dev make
$ sudo gem install git_stats
```
## Usage

### Generator

```
$ git_stats
Commands:
  git_stats generate        # Generates the statistics of a repository
  git_stats help [COMMAND]  # Describe available commands or one specific command
```
