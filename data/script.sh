#!/bin/bash

echo Generation donnees;
echo nombre de commit par annee;

echo "Année,Nombre de commit" > nbCommitYear.csv
git log --pretty='format:%cd' --date=format:'%Y' | sort | uniq -c | awk '{print $2","$1}' >> nbCommitYear.csv

echo top 10 des auteurs et leur nombre de commit par annee

echo "Année,Nombre de commit,Auteur" > top10AuthorActivities.csv
declare -a authors

while read line;
do
	authors+=( "$line" )
done < <(git shortlog -sn | head -10 | awk '{$1="";print $0}' )


for author in "${authors[@]}"
do
    for year in {2007..2020}
    do
		declare commit=`git shortlog -sn --all --since="01 Jan $year" --until="31 Dec $year" --author="$author" | awk '{print $1}'`
		echo $year,$commit,$author >> top10AuthorActivities.csv
    done
done;

echo top 5 des auteurs par annee

echo "Année,Nombre de commit,Auteur" > top5AuthorByYear.csv
for year in {2007..2020}
do
    git shortlog -sn --since="01 Jan $year" --until="31 Dec $year" | head -5 | while read f1 rest;
    do
		echo $rest,$year,$f1>> top5AuthorByYear.csv
    done
done;


#git log --shortstat --author="Marcin Olichwirowicz" | grep -E "fil(e|es) changed" | awk '{files+=$1; inserted+=$4; deleted+=$6} END {print "files changed: ", files, "lines inserted: ", inserted, "lines deleted: ", deleted }'
